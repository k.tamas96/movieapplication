package com.example.movieapplication.ui.offline

import android.content.Context
import com.example.movieapplication.base.BaseView
import com.example.movieapplication.persistence.tables.MovieItem

interface IOfflineView : BaseView {
    fun makeRatingString(average: String, count: String): CharSequence?
    fun deleteMovie(movieItem: MovieItem)
    fun getAppContext(): Context
    fun addMovies(movies: List<MovieItem>)
}
package com.example.movieapplication.ui.offline

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.movieapplication.MovieApplication
import com.example.movieapplication.R
import com.example.movieapplication.persistence.tables.MovieItem
import com.example.movieapplication.ui.offline.adapter.OfflineAdapter
import kotlinx.android.synthetic.main.activity_offline_movies.*
import javax.inject.Inject

class OfflineActivity : AppCompatActivity(), IOfflineView {

    @Inject
    lateinit var presenter: OfflinePresenter

    private var adapter: OfflineAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_offline_movies)
        initComponents()
        initRecyclerView()
    }

    init {
        MovieApplication.movieComponent.inject(this)
    }

    override fun initComponents() {
        adapter = OfflineAdapter(this)
        presenter.addView(this)

    }

    private fun initRecyclerView() {
        offline_recycler_view.layoutManager = LinearLayoutManager(this)
        offline_recycler_view.adapter = adapter
    }

    override fun makeRatingString(average: String, count: String): CharSequence? {
        return getString(R.string.search_result_rating, average, count)
    }

    override fun addMovies(movies: List<MovieItem>) {
        adapter?.addAllMovies(movies)
    }

    override fun deleteMovie(movieItem: MovieItem) {
        presenter.deleteMovie(movieItem)
        adapter?.deleteMovie(movieItem)
    }

    fun deleteAllMovies(v: View?) {
        v?.let {
            presenter.deleteAllMovie()
            adapter?.deleteAllMovie()
        }
    }

    override fun getAppContext(): Context {
        return applicationContext
    }
}

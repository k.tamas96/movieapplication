package com.example.movieapplication.ui.offline.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapplication.R
import com.example.movieapplication.persistence.tables.MovieItem
import com.example.movieapplication.ui.offline.IOfflineView
import kotlinx.android.synthetic.main.offline_item.view.*

class OfflineAdapter(private var view: IOfflineView) : RecyclerView.Adapter<OfflineAdapter.OfflineViewHolder>() {

    private var movies : ArrayList<MovieItem> = ArrayList()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): OfflineViewHolder {
        val itemView = LayoutInflater.from(p0.context)
            .inflate(R.layout.offline_item,p0,false)
        return OfflineViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: OfflineViewHolder, position: Int) {
        val item = movies[position]
        holder.titleTv.text = item.title
        holder.releaseDateTv.text = item.releaseDate
        holder.ratingTv.text = view.makeRatingString(item.voteAverage.toString(),item.voteCount.toString())
        holder.movieId =  item.id.toString()
        holder.deleteButton.setOnClickListener {
            view.deleteMovie(item)
        }
    }

    fun addAllMovies(movies: List<MovieItem>) {
        this.movies.clear()
        this.movies.addAll(movies)
        notifyDataSetChanged()
    }

    fun deleteMovie(movieItem: MovieItem) {
        val position = this.movies.indexOf(movieItem)
        this.movies.remove(movieItem)
        notifyItemRemoved(position)
    }

    fun deleteAllMovie() {
        this.movies.clear()
        notifyDataSetChanged()
    }


    class OfflineViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val titleTv: TextView = itemView.title_text_view_offline
        val releaseDateTv: TextView = itemView.release_date_text_view_offline
        val ratingTv: TextView = itemView.rating_text_view_offline
        var movieId : String? = null
        val deleteButton: ImageButton = itemView.button_delete_offline
    }
}
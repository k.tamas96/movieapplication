package com.example.movieapplication.ui.offline

import com.example.movieapplication.base.BasePresenter
import com.example.movieapplication.persistence.database.MovieDatabase
import com.example.movieapplication.persistence.tables.MovieItem
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class OfflinePresenter(private var database: MovieDatabase) : BasePresenter<IOfflineView> {

    private var view: IOfflineView? = null

    override fun addView(view: IOfflineView) {
        this.view = view
        loadDatabase()
    }

    private fun loadDatabase() {
        doAsync {
            val movies = getMoviesAsync()
            uiThread {
                view?.addMovies(movies)
            }
        }
    }

    override fun destroyView() {
        view = null
    }

    fun deleteMovie(movieItem: MovieItem) {
        doAsync {
            deleteMovieAsync(movieItem)
        }
    }

    private fun getMoviesAsync(): List<MovieItem> {
        return database.movieItemDao().getAllMovie()

    }

    private fun deleteMovieAsync(movieItem: MovieItem) {
        database.movieItemDao().deleteMovie(movieItem)
    }

    fun deleteAllMovie() {
        doAsync {
            deleteAllMovieAsync()
        }
    }

    private fun deleteAllMovieAsync() {
        database.movieItemDao().deleteAllMovie()
    }
}
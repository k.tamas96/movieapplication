package com.example.movieapplication.ui.search

import com.example.movieapplication.MovieApplication
import com.example.movieapplication.base.BasePresenter
import com.example.movieapplication.network.client.ApiClient
import com.example.movieapplication.network.model.SearchResultItem
import com.example.movieapplication.persistence.database.MovieDatabase
import com.example.movieapplication.persistence.tables.MovieItem
import com.google.firebase.messaging.FirebaseMessaging
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jetbrains.anko.doAsync
import javax.inject.Inject

class SearchPresenter(private var database: MovieDatabase) : BasePresenter<ISearchScreen> {

    private var view: ISearchScreen? = null

    @Inject
    lateinit var client: ApiClient

    @Inject
    lateinit var subscriptions: CompositeDisposable

    init {
        MovieApplication.movieComponent.inject(this)
    }


    override fun addView(view: ISearchScreen) {
        this.view = view
        subscribeToTopic()
    }


    private fun subscribeToTopic() {
        FirebaseMessaging.getInstance().subscribeToTopic("my_topic")
    }

    suspend fun getMovies(title: String) {
        try {
            val response = client.getMovieList(title)
            if (response.isSuccessful) {
                val result = response.body()
                result?.let {
                    if (it.total_results > 0) {
                        view?.addMovies(it.results)
                        saveMovies(it.results)
                    } else {
                        view?.noResult()
                    }
                }
            } else {
                view?.showNoConnectionMessage()
            }
        } catch (e: Exception) {
            view?.showNoConnectionMessage()
        }
    }

    private suspend fun saveMovies(movies: ArrayList<SearchResultItem>) {
        withContext(Dispatchers.IO) {
            database.movieItemDao().deleteAllMovie()
            for (movie in movies) {
                val movieItem = MovieItem(
                    movie.id?.toLong(),
                    movie.title,
                    movie.posterPath,
                    movie.releaseDate,
                    movie.voteCount,
                    movie.voteAverage
                )
                database.movieItemDao().addMovie(movieItem)
            }
        }
    }

    override fun destroyView() {
        subscriptions.clear()
        view = null
    }
}
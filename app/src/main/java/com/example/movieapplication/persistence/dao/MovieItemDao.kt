package com.example.movieapplication.persistence.dao

import androidx.room.*
import com.example.movieapplication.persistence.tables.MovieItem

@Dao
interface MovieItemDao {
    @Query("SELECT * from Movies")
    fun getAllMovie() : List<MovieItem>

    @Query("SELECT * from Movies where title LIKE :title")
    fun getMovieByTitle(title: String) : MovieItem

    @Insert
    fun addMovie(movieItem: MovieItem)

    @Delete
    fun deleteMovie(movieItem: MovieItem)

    @Update
    fun updateMovie(movieItem: MovieItem)

    @Query("DELETE from Movies")
    fun deleteAllMovie()
}
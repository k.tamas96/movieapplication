package com.example.movieapplication.persistence.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.movieapplication.persistence.dao.MovieItemDao
import com.example.movieapplication.persistence.tables.MovieItem


@Database(entities = [MovieItem::class], version = 1)
abstract class MovieDatabase : RoomDatabase() {
    abstract fun movieItemDao() : MovieItemDao
}
package com.example.movieapplication.persistence.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Movies")
data class MovieItem(
        @PrimaryKey var id: Long?,
        @ColumnInfo(name = "title") var title: String?,
        @ColumnInfo(name = "poster_url") var posterPath: String?,
        @ColumnInfo(name = "releaseDate") var releaseDate: String?,
        @ColumnInfo(name = "voteCount") var voteCount: Int?,
        @ColumnInfo(name = "voteAverage") var voteAverage: Double?
)
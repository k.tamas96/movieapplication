package com.example.movieapplication.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import androidx.core.app.NotificationCompat
import com.example.movieapplication.R
import com.example.movieapplication.ui.offline.OfflineActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MessageService : FirebaseMessagingService() {

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        message.let {
            val title = it.data["title"]
            val body = it.data["body"]

            val intent = Intent(applicationContext, OfflineActivity::class.java)
            val pendingIntent =
                PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            if (Build.VERSION.SDK_INT >= 26) {
                val channel = NotificationChannel(
                    "movie_app",
                    "movie_app",
                    NotificationManager.IMPORTANCE_DEFAULT
                )
                channel.description = "Channel description"
                notificationManager.createNotificationChannel(channel)
            }

            val notificationBuilder = NotificationCompat.Builder(this, "movie_app")
                .setSmallIcon(R.drawable.ic_beach_access_black_24dp)
                .setLargeIcon(BitmapFactory.decodeResource(applicationContext.resources,R.mipmap.ic_launcher))
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
            notificationManager.notify(1, notificationBuilder.build())
        }
    }
}

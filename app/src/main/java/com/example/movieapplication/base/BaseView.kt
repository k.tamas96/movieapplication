package com.example.movieapplication.base

interface BaseView {
    fun initComponents()
}
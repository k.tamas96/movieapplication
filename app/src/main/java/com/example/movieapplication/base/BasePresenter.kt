package com.example.movieapplication.base

import com.example.movieapplication.persistence.database.MovieDatabase

interface BasePresenter<T> {
    fun addView(view: T)
    fun destroyView()
}
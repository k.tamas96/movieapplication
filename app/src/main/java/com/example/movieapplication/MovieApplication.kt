package com.example.movieapplication

import android.app.Application
import com.example.movieapplication.dagger.AppComponent
import com.example.movieapplication.dagger.DaggerAppComponent
import com.example.movieapplication.dagger.PresenterModule
import com.example.movieapplication.dagger.ServiceModule

class MovieApplication : Application() {

    companion object {
        lateinit var movieComponent: AppComponent
    }

    init {
        try {
            movieComponent = initDagger()
        } catch (e: Exception) {
            e.toString()
        }
    }


    private fun initDagger(): AppComponent =
        DaggerAppComponent.builder()
            .presenterModule(PresenterModule(this))
            .serviceModule(ServiceModule())
            .build()
}
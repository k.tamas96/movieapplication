package com.example.movieapplication.dagger

import android.content.Context
import androidx.room.Room
import com.example.movieapplication.persistence.database.MovieDatabase
import com.example.movieapplication.ui.details.DetailsPresenter
import com.example.movieapplication.ui.offline.OfflinePresenter
import com.example.movieapplication.ui.search.SearchPresenter
import dagger.Module
import dagger.Provides


@Module
class PresenterModule(private var context: Context) {

    @Provides
    fun provideSearchPresenter() : SearchPresenter {
        return SearchPresenter(initDatabase())
    }

    @Provides
    fun provideDetailsPresenter() : DetailsPresenter = DetailsPresenter()

    @Provides
    fun provideOfflinePresenter() : OfflinePresenter {
        return OfflinePresenter(initDatabase())
    }

    private fun initDatabase(): MovieDatabase {
        return Room.databaseBuilder(context,
            MovieDatabase::class.java,
            "movie-list.db").build()
    }
}